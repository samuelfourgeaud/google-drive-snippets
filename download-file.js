/**
 * Downloading a file from a Google Shared Drive
 */

const { google } = require("googleapis");
const credentials = require("./credentials.json");
const fs = require("fs");
const path = require("path");
const os = require("os");
const uuid = require("uuid");

const scopes = ["https://www.googleapis.com/auth/drive.file"];

const auth = new google.auth.JWT(
  credentials.client_email,
  null,
  credentials.private_key,
  scopes
);
const drive = google.drive({
  version: "v3",
  auth,
});

/**
 * DEVELOPER TODO: Change the file id that will be downloaded
 */
var fileId = "11SmgULb84uVwNNeZ9MyaGsOmk29_FFG7";

var dest = fs.createWriteStream("./photo.png");

drive.files
  .get({ fileId, alt: "media" }, { responseType: "stream" })
  .then(res => {
    const filePath = path.join(os.tmpdir(), uuid.v4());
    console.log(`writing to ${filePath}`);
    let progress = 0;

    res.data
      .on("end", () => {
        console.log("Done downloading file.");
      })
      .on("error", err => {
        console.error("Error downloading file.");
        reject(err);
      })
      .on("data", d => {
        progress += d.length;
        if (process.stdout.isTTY) {
          process.stdout.clearLine();
          process.stdout.cursorTo(0);
          process.stdout.write(`Downloaded ${progress} bytes`);
        }
      })
      .pipe(dest);
  });
