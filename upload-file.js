const { google } = require("googleapis");
const credentials = require("./credentials.json");
const scopes = ["https://www.googleapis.com/auth/drive"];
const fs = require("fs");

const auth = new google.auth.JWT(
  credentials.client_email,
  null,
  credentials.private_key,
  scopes
);
const drive = google.drive({
  version: "v3",
  auth,
});

/**
 * DEVELOPER TODO: Change the folder/Drive id where the file will be uploaded
 */
const uploadFolderId = "0ADH1HdVIfk0FUk9PVA";

// Name of the file that will be created on the Shared Drive
const uploadFileName = "file-to-upload.txt";

const fileMetadata = {
  name: "file-uploaded.txt",
  parents: [uploadFolderId],
};
var media = {
  mimeType: "text/plain",
  body: fs.createReadStream(uploadFileName),
};
drive.files.create({
  resource: fileMetadata,
  media: media,
  fields: "id",
  supportsAllDrives: true,
  includeItemsFromAllDrives: true,
});
