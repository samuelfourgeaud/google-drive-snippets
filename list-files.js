/**
 * Listing all files in a Google Shared Drive
 */

const { google } = require("googleapis");
const credentials = require("./credentials.json");
const scopes = ["https://www.googleapis.com/auth/drive.file"];
const auth = new google.auth.JWT(
  credentials.client_email,
  null,
  credentials.private_key,
  scopes
);
const drive = google.drive({
  version: "v3",
  auth,
});

drive.files.list(
  {
    supportsAllDrives: true,
    includeItemsFromAllDrives: true,
    corpora: "allDrives",
  },
  (err, res) => {
    if (err) throw err;
    const files = res.data.files;
    if (files.length) {
      files.map(file => {
        console.log(file);
      });
    } else {
      console.log("No files found");
    }
  }
);
